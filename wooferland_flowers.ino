// MultiArrays - see https://github.com/FastLED/FastLED/wiki/Multiple-Controller-Examples for more info on
// using multiple controllers.  In this example, we're going to set up three NEOPIXEL strips on three
// different pins, each strip getting its own CRGB array to be played with

#include "FastLED.h"


#define NUM_LEDS_PER_STRIP 54

#define NUM_STRIPS 5

uint8_t brightness = 255;

CRGB flower_1[NUM_LEDS_PER_STRIP];


CRGBPalette16 palette[] = {PartyColors_p, OceanColors_p, LavaColors_p, ForestColors_p, HeatColors_p};
TBlendType currentBlending = LINEARBLEND;

uint8_t speedArray[] = {1,1, 3,1,2};

boolean clockwiseRotation[ NUM_STRIPS ];

// For mirroring strips, all the "special" stuff happens just in setup.  We
// just addLeds multiple times, once for each strip
void setup() {
  // tell FastLED there's 60 NEOPIXEL leds on pin 10
  FastLED.addLeds<NEOPIXEL, 8>(flower_1, NUM_LEDS_PER_STRIP);


  FastLED.setBrightness(  brightness );

  set_max_power_in_volts_and_milliamps   ( 5,1000 );
}

void loop() {

    static uint8_t startIndex[] = {0,0,0,0,0};
    startIndex[0] = startIndex[0] + speedArray[0]; /* motion speed */

    FillLEDsFromPaletteColors1( startIndex[0] );

    FastLED.show();
    FastLED.delay(100);

    for (size_t i = 0; i < NUM_STRIPS; i++) {
        switchPalette( i );
        switchSpeed( i );
    }

}


void FillLEDsFromPaletteColors1( uint8_t colorIndex ) {

    for( int i = 0; i < NUM_LEDS_PER_STRIP; i++) {
        flower_1[i] = ColorFromPalette( palette[0], colorIndex, brightness, currentBlending);
        colorIndex += 3;
    }
}



void switchPalette( uint8_t strip ) {

     CRGBPalette16 prevPattern = palette[ strip ];

    int switchToken = random(0,8000);

    switch ( switchToken ) {

        case 0:
            if ( checkDuplicatePalette( PartyColors_p )) {
                palette[ strip ] = PartyColors_p;
            }
            break;
        case 1:
            if ( checkDuplicatePalette( OceanColors_p )) {
                palette[ strip ] = OceanColors_p;
            }
            break;
        case 2:
            if ( checkDuplicatePalette( ForestColors_p )) {
                palette[ strip ] = ForestColors_p;
            }
            break;
        case 3:
            if ( checkDuplicatePalette( CloudColors_p )) {
                palette[ strip ] = CloudColors_p;
            }
            break;
        case 4:
            if ( checkDuplicatePalette( RainbowColors_p )) {
                palette[ strip ] = RainbowColors_p;
            }
            break;
        case 5:
            if ( checkDuplicatePalette( HeatColors_p )) {
                palette[ strip ] = HeatColors_p;
            }
            break;
        case 6:
            if ( checkDuplicatePalette( LavaColors_p )) {
                palette[ strip ] = LavaColors_p;
            }
            break;
        case 7:
            if ( checkDuplicatePalette( RainbowStripeColors_p )) {
                palette[ strip ] = RainbowStripeColors_p;
            }
            break;

    }

    // for (size_t i = 0; i < sizeof( palette ); i++) {
    //     if ( i != strip || palette[ strip ] == palette[ i ] ) {
    //         palette[ strip ] = prevPattern;
    //     }
    // }


}

boolean checkDuplicatePalette( CRGBPalette16 pal ) {

    for (size_t i = 0; i < NUM_STRIPS; i++) {
        if ( palette[ i ] == pal) {
            return false;
        }
        else {
            return true;
        }
    }
}

void switchSpeed( uint8_t strip ) {

    int switchToken = random(0,4000);

    switch ( switchToken ) {
        case 0:
            speedArray[ strip ] = 1;
            break;
        case 1:
            speedArray[ strip ] = 2;
            break;
        case 2:
            speedArray[ strip ] = 3;
            break;
        case 3:
            speedArray[ strip ] = 4;
            break;
        case 4:
            speedArray[ strip ] = 5;
            break;
        case 5:
            speedArray[ strip ] = 6;
            break;
        case 6:
            speedArray[ strip ] = 7;
            break;
        case 7:
            speedArray[ strip ] = 8;
            break;
        case 8:
            speedArray[ strip ] = 9;
            break;
        case 9:
            speedArray[ strip ] = 10;
            break;

    }
    if ( switchToken > 3900 ) {
        speedArray[ strip ] = random(1,3);
    }
}

void switchSirection( uint8_t strip ) {

    int switchToken = random(0,4000);

    switch ( switchToken ) {
        case 0:
            clockwiseRotation[ strip ] = !clockwiseRotation[ strip ];
            break;
        case 1:
            clockwiseRotation[ strip ] = !clockwiseRotation[ strip ];
            break;
        case 2:
            clockwiseRotation[ strip ] = !clockwiseRotation[ strip ];
            break;
        case 3:
            clockwiseRotation[ strip ] = !clockwiseRotation[ strip ];
            break;
        case 4:
            clockwiseRotation[ strip ] = !clockwiseRotation[ strip ];
            break;
        case 5:
            clockwiseRotation[ strip ] = !clockwiseRotation[ strip ];
            break;
        case 6:
            clockwiseRotation[ strip ] = !clockwiseRotation[ strip ];
            break;

    }
    if ( switchToken > 3800 ) {
        speedArray[ strip ] = random(1,3);
    }
}
